# This is repository for the CMU 246 XIS 2024 course.

## This repository contains the following projects:
- [Calculator](calculator/README.md)
- [Exercises](exercises/README.md)
### Clone code from this repository
```bash
git clone https://gitlab.com/ngtuonghy-duytanuniversity/CMU-246-XIS-2024.git
```