# Calculator Project

## Dependencies

- `Java 8 or higher`
- `Maven 3.4.1`

## Description
- This is a simple calculator project that can perform basic arithmetic operations.

### Requirements:
- Read two numbers from the user.
- Read the operation to be performed from the user.
- Perform the operation and display the result.
- if the user enters text instead of a number, the program should display an error message.

### Test Cases:
- Test Case 1:
    - Input: 5, +, 3
    - Output: 8
    - Description: The sum of 5 and 3 is 8.
    - Status: Passed
- Test Case 2:
    - Input: 5, -, 3
    - Output: 2
    - Description: The difference of 5 and 3 is 2.
    - Status: Passed
- Test Case 3:
    - Input: 5, *, 3
    - Output: 15
    - Description: The product of 5 and 3 is 15.
    - Status: Passed
- Test Case 4:
    - Input: 5, /, 0
    - Output: Cannot divide by zero
    - Status: Passed
    - Description: The division of 5 by 0 is not possible.
    - Status: Passed
- Test Case 5:
    - input: 5 , +, 3, =, =
    - Output: 11
    - Description: The sum of 5 and 3 is 8. The result is then added to 3 to get 11.
    - Status: Passed