package NGUYENTUONGHY;

import com.formdev.flatlaf.FlatIntelliJLaf;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class MainFrame implements ActionListener {
    double num1 = 0, num2 = 0, result = 0;
    String selectedOperator = "";

    JFrame frame = new JFrame();
    JLabel label = new JLabel();
    JTextField textField = new JTextField();
    JPanel panel = new JPanel();

    JButton addButton, subButton, mulButton, divButton; // buttons
    JButton dotButton, equalButton, delButton, clrButton, deleteButton, prButton, amduongButton;

    JButton[] numberButtons = new JButton[10]; // 0-9
    JButton[] functionButtons = new JButton[11];


    MainFrame() {

        initGui();
        addComponents();
        frame.setVisible(true);
    }

    Font myFont = new Font("Times New Roman", Font.BOLD, 23); // global font
    Font fontButton = new Font("Times New Roman", Font.BOLD, 21); // global font

    public void initGui() {
        frame.setTitle("Calculator code by Nguyen Tuong Hy");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(350, 500);
        frame.setLocationRelativeTo(null);
        ImageIcon image = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("logo.png")));
        frame.setIconImage(image.getImage());
        frame.setLayout(null);
        frame.add(panel);
        frame.setResizable(false);

    }

    public void addComponents() {
        showOutput();
        keyboardCalc();
    }

//    public void showHistory() {
//      // TODO: Implement this method to show the history of the calculation
//    }

    public void showOutput() {
        textField.setBounds(20, 55, 300, 60);
        textField.setFont(new Font("Times New Roman", Font.BOLD, 40));
        textField.setHorizontalAlignment(textField.RIGHT);
        textField.setEnabled(false);
        textField.setDisabledTextColor(Color.BLACK);
        frame.add(textField);

        label.setBounds(20, 20, 300, 30);
        label.setFont(myFont);
        label.setHorizontalAlignment(label.RIGHT);
        frame.add(label);

    }

    public void keyboardCalc() {

        for (int i = 0; i < 10; i++) {
            numberButtons[i] = new JButton(String.valueOf(i));
            numberButtons[i].addActionListener(this);
            numberButtons[i].setFont(myFont);
            numberButtons[i].setFocusable(false);
        }

        panel.setBounds(20, 150, 300, 300);
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        gridBagConstraints.anchor = GridBagConstraints.CENTER;
        gridBagConstraints.fill = GridBagConstraints.BOTH;


        addButton = new JButton("+");
        subButton = new JButton("-");
        mulButton = new JButton("*");
        divButton = new JButton("/");
        dotButton = new JButton(".");
        equalButton = new JButton("=");
        delButton = new JButton("X");
        clrButton = new JButton("CE");
        deleteButton = new JButton("C");
        prButton = new JButton("%");
        amduongButton = new JButton("-/+");

        functionButtons[0] = addButton;
        functionButtons[1] = subButton;
        functionButtons[2] = mulButton;
        functionButtons[3] = divButton;
        functionButtons[4] = dotButton;
        functionButtons[5] = equalButton;
        functionButtons[6] = delButton;
        functionButtons[7] = clrButton;
        functionButtons[8] = deleteButton;
        functionButtons[9] = prButton;
        functionButtons[10] = amduongButton;

        for (int i = 0; i < 11; i++) {
            functionButtons[i].setPreferredSize(new Dimension(50, 50));
            functionButtons[i].addActionListener(this);
            functionButtons[i].setFont(fontButton);
            functionButtons[i].setFocusable(false);
        }

        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;

        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        panel.add(clrButton, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        panel.add(deleteButton, gridBagConstraints);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        panel.add(prButton, gridBagConstraints);
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        panel.add(divButton, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        panel.add(numberButtons[7], gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        panel.add(numberButtons[8], gridBagConstraints);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        panel.add(numberButtons[9], gridBagConstraints);
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        panel.add(mulButton, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        panel.add(numberButtons[4], gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        panel.add(numberButtons[5], gridBagConstraints);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        panel.add(numberButtons[6], gridBagConstraints);
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        panel.add(subButton, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        panel.add(numberButtons[1], gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        panel.add(numberButtons[2], gridBagConstraints);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        panel.add(numberButtons[3], gridBagConstraints);
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        panel.add(addButton, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        panel.add(dotButton, gridBagConstraints);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        panel.add(numberButtons[0], gridBagConstraints);
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        equalButton.setBackground(Color.cyan);
        panel.add(equalButton, gridBagConstraints);

    }

    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < numberButtons.length; i++) {
            if (e.getSource() == numberButtons[i]) {
                textField.setText(textField.getText().concat(String.valueOf(i)));
            }
        }


        if (e.getSource() == dotButton && !textField.getText().isEmpty())
            textField.setText(textField.getText().concat("."));


        if (selectedOperator.isEmpty() && !textField.getText().isEmpty()) {
            num1 = Double.parseDouble(textField.getText());
        }


        if (!textField.getText().isEmpty() && !textField.getText().contains("Zero")) {
            if (!label.getText().contains("=")) {
                num2 = Double.parseDouble(textField.getText());
            } else {
                num1 = Double.parseDouble(textField.getText());
            }
        }

        if (e.getSource() == addButton) {
            textField.setText("");
            selectedOperator = "+";
            if (num1 % 1 == 0) label.setText((int) num1 + " + ");
            else label.setText(num1 + " +");

        }

        if (e.getSource() == subButton) {
            textField.setText("");
            selectedOperator = "-";
            if (num1 % 1 == 0) label.setText((int) num1 + " - ");
            else label.setText(num1 + " - ");
        }

        if (e.getSource() == mulButton) {

            textField.setText("");
            selectedOperator = "*";
            if (num1 % 1 == 0) label.setText((int) num1 + " * ");
            else label.setText(num1 + " * ");
        }

        if (e.getSource() == divButton) {
            selectedOperator = "/";
            textField.setText("");
            if (num1 % 1 == 0) label.setText((int) num1 + " / ");
            else label.setText(num1 + " / ");
        }

        if (e.getSource() == equalButton) {
            switch (selectedOperator) {
                case "+" -> result = num1 + num2;
                case "-" -> result = num1 - num2;
                case "*" -> result = num1 * num2;
                case "/" -> {
                    if (Double.parseDouble(textField.getText()) == 0) {
                        textField.setText("Can't divide by Zero!");
                        return;
                    }
                    result = num1 / Double.parseDouble(textField.getText());
                }
                default -> {
                    result = num1;
                }
            }

            if (!selectedOperator.isEmpty()) {
                if (num1 % 1 == 0 && num2 % 1 != 0) {
                    label.setText((int) num1 + " " + selectedOperator + " " + num2 + " = ");
                } else if (num1 % 1 != 0 && num2 % 1 == 0) {
                    label.setText(num1 + " " + selectedOperator + " " + (int) num2 + " = ");
                } else if (num1 % 1 == 0 && num2 % 1 == 0) {
                    label.setText((int) num1 + " " + selectedOperator + " " + (int) num2 + " = ");
                } else {
                    label.setText(num1 + " " + selectedOperator + " " + num2 + " = ");
                }

                if (result % 1 == 0) textField.setText(String.valueOf((int) result));
                else textField.setText(String.valueOf(result));

                num1 = result;
                result = 0;
            }
        }

        if (e.getSource() == clrButton) {
            textField.setText("");
            label.setText("");
            if (!selectedOperator.isEmpty()) {
                selectedOperator = "";
            }
            num1 = 0;
            num2 = 0;
            result = 0;
        }
        if (e.getSource() == deleteButton) {
            textField.setText("");

        }
//        if (e.getSource() == deleteButton) {
//            String string = textField.getText();
//            textField.setText("");
//            for (int i = 0; i < string.length() - 1; i++) {
//                textField.setText(textField.getText() + string.charAt(i));
//            }
//
//        }
        if (e.getSource() == prButton) {
            double temp = Double.parseDouble(textField.getText());
            temp /= 100;
            textField.setText(String.valueOf(temp));
        }

    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new FlatIntelliJLaf());
        } catch (Exception ex) {
            System.err.println("Failed to initialize LaF");
        }
        UIManager.put("Button.arc", 7);
        new MainFrame();
    }
}

