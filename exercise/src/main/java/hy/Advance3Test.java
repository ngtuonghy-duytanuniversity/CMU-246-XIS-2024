package hy;
import  org.junit.Test;
import nguyen.Advance3;
public class Advance3Test {
   @Test
    public void fibonacciTest(){
        Advance3 advance3 = new Advance3();
        assert advance3.fibonacci(9) == 34;
        assert advance3.fibonacci(10) == 55;
        assert advance3.fibonacci(11) == 89;
    }
}
