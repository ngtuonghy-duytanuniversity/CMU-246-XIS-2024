package hy;

import nguyen.Calculator1;
import org.junit.Test;

public class Calculator1Test {
    @Test
    public void addTestResult() {
        Calculator1 calculator1 = new Calculator1();
        assert calculator1.add(1, 2) == 3;
    }
    @Test
    public void addTestNumberLargerThanMaxInt() {
        Calculator1 calculator1 = new Calculator1();
        assert calculator1.add(Integer.MAX_VALUE, 1) == Integer.MIN_VALUE;
    }
    @Test
    public void addTestNumberSmallerThanMinInt() {
        Calculator1 calculator1 = new Calculator1();
        assert calculator1.add(Integer.MIN_VALUE, -1) == Integer.MAX_VALUE;
    }

    public static void main(String[] args) {
        Calculator1Test calculator1Test = new Calculator1Test();
        calculator1Test.addTestResult();
        calculator1Test.addTestNumberLargerThanMaxInt();
        calculator1Test.addTestNumberSmallerThanMinInt();
    }

}
