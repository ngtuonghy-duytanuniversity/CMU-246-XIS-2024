package hy;

import nguyen.Sort1;
import org.junit.Test;

public class Sort1Test {
    @Test
    public void number1GreaterThanNumber2Test() {
        Sort1.number1 = 5;
        Sort1.number2 = 3;
        Sort1.sortAsc();
        assert Sort1.number1 == 3;
        assert Sort1.number2 == 5;
    }

    @Test
    public void number1LessThanNumber2Test() {
        Sort1.number1 = 3;
        Sort1.number2 = 5;
        Sort1.sortAsc();
        assert Sort1.number1 == 3;
        assert Sort1.number2 == 5;
    }

}
