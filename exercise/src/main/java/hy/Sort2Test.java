package hy;

import nguyen.Sort2;
import org.junit.Test;

public class Sort2Test {
    @Test
    public void number1GreaterThanNumber2Test() {
        Sort2 sort2 = new Sort2();
        sort2.number1 = 5;
        sort2.number2 = 3;
        sort2.sortDesc();
        assert sort2.number1 == 5;
        assert sort2.number2 == 3;
    }
    @Test
    public void number1LessThanNumber2Test() {
        Sort2 sort2 = new Sort2();
        sort2.number1 = 3;
        sort2.number2 = 5;
        sort2.sortDesc();
        assert sort2.number1 == 5;
        assert sort2.number2 == 3;
    }
}
