package hy;

import nguyen.Advance2;
import org.junit.Test;

public class Advance2Test {
    @Test
    public void sumTest() {
        Advance2 advance2 = new Advance2();
        assert advance2.sum(5765) == 23;
    }
}
