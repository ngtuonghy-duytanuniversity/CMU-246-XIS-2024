package hy;

import nguyen.Calculator2;
import org.junit.Test;

public class Calculator2Test {
    @Test
    public void resultTest() {
        Calculator2 calculator = new Calculator2();
        calculator.number1 = 5;
        calculator.number2 = 3;
        assert calculator.result == 2;
    }

    @Test
    public void numberLargerThanMaximumTheIntTest() {
        Calculator2 calculator = new Calculator2();
        calculator.number1 = Integer.MAX_VALUE;
        calculator.number2 = 1;
        assert calculator.result == Integer.MAX_VALUE;
    }

    @Test
    public void numberSmallerThanMinimumTheIntTest() {
        Calculator2 calculator = new Calculator2();
        calculator.number1 = Integer.MIN_VALUE;
        calculator.number2 = -1;
        assert calculator.result == Integer.MIN_VALUE;
    }
}
