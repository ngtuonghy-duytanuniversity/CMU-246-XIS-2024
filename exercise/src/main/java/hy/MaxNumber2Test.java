package hy;

import nguyen.MaxNumber2;
import org.junit.Test;

public class MaxNumber2Test {
  @Test
    public void firstNumberIsMax() {
        MaxNumber2 maxNumber2 = new MaxNumber2();
        maxNumber2.number1 = 10;
        maxNumber2.number2 = 3;
        assert maxNumber2.max2() == 10;
    }
    @Test
    public void secondNumberIsMax() {
        MaxNumber2 maxNumber2 = new MaxNumber2();
        maxNumber2.number1 = 1;
        maxNumber2.number2 = 30;
        assert maxNumber2.max2() == 30;
    }
}
