package hy;

import nguyen.MaxNumber1;
import org.junit.Test;

public class MaxNumber1Test {
    @Test
    public void fistNumberIsMax() {
        MaxNumber1 maxNumber1 = new MaxNumber1(1, 3, 10);
        assert maxNumber1.max3() == 10;
    }
    @Test
    public void secondNumberIsMax() {
        MaxNumber1 maxNumber1 = new MaxNumber1(1, 30, 10);
        assert maxNumber1.max3() == 30;
    }
    @Test
    public void thirdNumberIsMax() {
        MaxNumber1 maxNumber1 = new MaxNumber1(100, 30, 10);
        assert maxNumber1.max3() == 100;
    }
}
