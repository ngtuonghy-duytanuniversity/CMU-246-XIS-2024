package hy;

import nguyen.Calculator3;
import org.junit.Test;

public class Calculator3Test {
    @Test
    public void resultTest() {
        Calculator3 calculator = new Calculator3(5, 3);
        assert calculator.mul() == 15;
    }

    @Test
    public void numberLargerThanMaximumTheIntTest() {
        Calculator3 calculator = new Calculator3(Integer.MAX_VALUE, 1);
        assert calculator.mul() == Integer.MAX_VALUE;
    }

    @Test
    public void numberSmallerThanMinimumTheIntTest() {
        Calculator3 calculator = new Calculator3(Integer.MIN_VALUE, -1);
        assert calculator.mul() == Integer.MIN_VALUE;
    }
}
