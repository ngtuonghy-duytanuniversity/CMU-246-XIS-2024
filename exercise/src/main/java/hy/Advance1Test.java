package hy;

import org.junit.Test;
import nguyen.Advance1;
public class Advance1Test {
   @Test
    public void advanceTest(){
        Advance1 advance1 = new Advance1();
        assert advance1.USCLN(6, 9) == 3;
        assert advance1.BSCNN(6, 9) == 18;
   }
}
