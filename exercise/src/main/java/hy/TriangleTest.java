package hy;

import nguyen.Triangle;
import org.junit.Test;

public class TriangleTest {
    @Test
    public void firstNumberIsMax() {
        Triangle triangle = new Triangle();
        triangle.setNumber1(10);
        triangle.setNumber2(3);
        triangle.setNumber3(5);
        assert triangle.maxLength() == 10;
    }
    @Test
    public void secondNumberIsMax() {
        Triangle triangle = new Triangle();
        triangle.setNumber1(1);
        triangle.setNumber2(30);
        triangle.setNumber3(5);
        assert triangle.maxLength() == 30;
    }
    @Test
    public void thirdNumberIsMax() {
        Triangle triangle = new Triangle();
        triangle.setNumber1(1);
        triangle.setNumber2(3);
        triangle.setNumber3(50);
        assert triangle.maxLength() == 50;
    }
}
