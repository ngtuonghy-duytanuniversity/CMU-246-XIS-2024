package hy;

import nguyen.SolveEquation;
import org.junit.Test;

public class SolveEquationTest {
  @Test
  public void multiRootsTest() {
    SolveEquation solveEquation = new SolveEquation();
    solveEquation.setNumber1(0);
    solveEquation.setNumber2(0);
    assert solveEquation.linearEquation().equals("Multi roots");
  }
  @Test
  public void oneRootTest() {
    SolveEquation solveEquation = new SolveEquation();
    solveEquation.setNumber1(1);
    solveEquation.setNumber2(0);
    assert solveEquation.linearEquation().equals("One root");
  }
}
