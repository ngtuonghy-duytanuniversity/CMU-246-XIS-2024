package hy;
import  org.junit.Test;
import nguyen.Advance4;
public class Advance4Test {
  @Test
    public void isPrimeNumberTest(){
      Advance4 advance4 = new Advance4();
        assert advance4.isPrimeNumber(2);
        assert advance4.isPrimeNumber(3);
        assert !advance4.isPrimeNumber(4);

    }
}