package hy;

import java.util.Calendar;
import java.util.Date;
import org.junit.Test;
import nguyen.Advance7;
public class Advance7Test {
 @Test
    public void tinhthuTest(){
        Advance7 advance7 = new Advance7();
        // NOTE: 1 is Sunday, 2 is Monday, 3 is Tuesday, 4 is Wednesday, 5 is Thursday, 6 is Friday, 7 is Saturday
        assert advance7.tinhThu(5,5,2024) == 1;
    }
}
