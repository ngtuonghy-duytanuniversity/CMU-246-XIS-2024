package hy;

import nguyen.Calculator4;
import org.junit.Test;

public class Calculator4Test {
    @Test
    public void resultTest() {
        Calculator4 calculator4 = new Calculator4();
        calculator4.setNumber1(10);
        calculator4.setNumber2(2);
        assert calculator4.getNumber1() == 10;
        assert calculator4.getNumber2() == 2;
    }

    @Test
    public void divisionZeroTest() {
        Calculator4 calculator4 = new Calculator4();
        calculator4.setNumber1(10);
        calculator4.setNumber2(0);
//       System.out.println(calculator4.div());
        assert calculator4.div() == 0;
    }

    @Test
    public void divisionReturnFloatTest() {
        Calculator4 calculator4 = new Calculator4();
        calculator4.setNumber1(5);
        calculator4.setNumber2(2);
        assert calculator4.div() == 2.5;
    }
}
