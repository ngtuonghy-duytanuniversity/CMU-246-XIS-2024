package REFACTORING;

public class CircleAreaCalculator {
    public double calculateArea(double radius) {
        double pi = 3.14;
        double area = pi * radius * radius;
        return area;
    }

    public double CircleAreaCalculatorRefactor(double radius) {
        return Math.PI * radius * radius;
    }
}
