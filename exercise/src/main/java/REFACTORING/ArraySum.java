package REFACTORING;

import java.util.Arrays;

public class ArraySum {
    public int sumArray(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    public int sumArrayRefactor(int[] arr) {
        return Arrays.stream(arr).sum();
    }
}
