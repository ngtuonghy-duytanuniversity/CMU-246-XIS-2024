package REFACTORING;

public class SquareRootCalcuIator {
    public double calculateSquareRoot(double number) {
        return Math.sqrt(number);
    }
    public double calculateSquareRootRefactor(double number) {
        return Math.sqrt(number);
    }
}
