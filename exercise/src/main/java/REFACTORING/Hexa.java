package REFACTORING;

public class Hexa {
    public void FourNumber() {
        char[] d = new char[]{48, 49, 50, 51, 52, 53,
                54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
        int Decimal = 0;
        for (int i = 0; i < 16; i++)
            for (int j = 0; j < 16; j++)
                for (int k = 0; k < 16; k++)
                    for (int l = 0; l < 16; l++) {
                        System.out.println("" + d[i]
                                + d[j] + d[k] + d[l] + "\t" + Decimal);
                        Decimal++;
                    }
    }

    public static void main(String a[]) {
        Hexa oc = new Hexa();
        System.out.println("Hexa\tDecimal");
        oc.FourNumber();
    }
}

class HexaRefactor {
    public void FourNumber() {
        int Decimal = 0;
        for (int decimalValue = 0; decimalValue < 65536; decimalValue++) {
            String hexValue = Integer.toHexString(decimalValue).toUpperCase(); // Convert to hexadecimal string
            hexValue = String.format("%4s", hexValue).replace(' ', '0'); // Pad with leading zeros if necessar
            // Print the hexadecimal value and corresponding decimal value
            System.out.println(hexValue + "\t" + Decimal);
            Decimal++;
        }
    }

    public static void main(String[] args) {
        Hexa hexaConverter = new Hexa();
        System.out.println("Hexa\tDecimal");
        hexaConverter.FourNumber();
    }

}