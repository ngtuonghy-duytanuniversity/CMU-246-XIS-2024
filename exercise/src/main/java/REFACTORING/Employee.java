package REFACTORING;

public class Employee {
    private String empName;
    private int empld;

    public Employee(String name, int id) {
        empName = name;
        empld = id;
    }
    public void displayEmployeeDetails() {
        System.out.println("Employee ID: " + empld);
        System.out.println("Employee Name: " + empName);
    }
}
