package nguyentuonghy;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.Math.sqrt;

public class NguyenTuongHy {
    private int Calsum() {
        int[] arr = {1, 5, 7, 3, 8};
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
//        return Arrays.stream(arr).sum();
        return sum;
    }

    @Test
    public void testCalum() {
        Assert.assertEquals(1 + 5 + 7 + 3 + 8, this.Calsum());
    }

    private boolean iPrime(int x) {
        if (x < 2) {
            return false;
        }
        for (int i = 2; i <= sqrt(x); i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }

    @Test
    public void testIPrime() {
        Assert.assertTrue(this.iPrime(7));
    }

    private String revstring() {
        String name = "hello";
        char[] charName = name.toCharArray();
        String revName = "";
        for (int i = charName.length - 1; i >= 0; i--) {
            revName += charName[i];
        }
        return revName;
    }

    @Test
    public void testRevstring() {
        Assert.assertEquals("olleh", this.revstring());
    }

    int calfactorial(int number) {
        int fact = 1;
        for (int i = number; i > 0; i--) {
            fact *= i;
        }
        return fact;
    }

    @Test
    public void testCalfactorial() {
        Assert.assertEquals(120, this.calfactorial(5));
    }

    private int fiMaxNum() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    @Test
    public void testFiMaxNum() {
        Assert.assertEquals(9, this.fiMaxNum());
    }

    public static void main(String[] args) {
        System.out.println("code by Nguyen Tuong Hy");
        NguyenTuongHy hy = new NguyenTuongHy();
        hy.testCalum();
        hy.testIPrime();
        hy.testRevstring();
        hy.testCalfactorial();
        hy.testFiMaxNum();
    }
}
