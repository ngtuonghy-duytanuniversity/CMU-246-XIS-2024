package cmu.nguyentuonghy.April.Day13;

public class SquareRootCalcuIator {
    public double calculateSquareRoot(double number) {
        return Math.sqrt(number);
    }

    public double calculateSquareRootRefactor(double number) {
        return Math.sqrt(number);
    }

}
