package nguyentuonghy.April.Day20;


public class FindMaxArray {
    int findMaxArray(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    public void TestFindMaxArray() {
        int[] arr = {0, 2, 3, 4, 5};
        assert this.findMaxArray(arr) == 5;
    }

    public static void main(String[] args) {
        FindMaxArray findMaxArray = new FindMaxArray();
        findMaxArray.TestFindMaxArray();
    }
}
